#define _CLC_OVERLOAD __attribute__((overloadable))
#define _CLC_DECL
#ifndef CLC_SPIRV
#define _CLC_DEF __attribute__((always_inline))
#else
#define _CLC_DEF
#endif
#define _CLC_INLINE __attribute__((always_inline)) inline
